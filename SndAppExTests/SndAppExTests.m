//
//  SndAppExTests.m
//  SndAppExTests
//
//  Created by Rob Baron on 5/27/14.
//  Copyright (c) 2014 RBB. All rights reserved.
//

#import "SndAppExTests.h"

@implementation SndAppExTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in SndAppExTests");
}

@end
