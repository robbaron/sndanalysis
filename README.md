# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This is just a project that I am using to learn Objective C/Cocoa programming

It reads a user selected file and displays the associated waveform.

Version: 0.01 

### How do I get set up? ###

I have included the x-code project file, so just compile within x-code.

### Contribution guidelines ###

There are better projects to contribute to, like audacity.

### Who do I talk to? ###

Rob Baron <robertbartlettbaron@gmail.com>