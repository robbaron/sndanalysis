//
//  sndapp_view.h
//  SndAppEx
//
//  Created by Rob Baron on 5/29/14.
//  Copyright (c) 2014 Rob Baron. All rights reserved.
//
#import <Cocoa/Cocoa.h>
#import "sndapp_AppDelegate.h"
@interface graph_view : NSView {
    double *data;
    long n;
}

- (void) drawAxis: (NSRect) bounds;
- (void) drawBorder: (NSRect) bounds;
- (void) set_data: (double *) data size:(long)n;
- (void) plot: (NSRect) bounds;

@end

@interface MyPoint : NSObject {
    NSPoint myNSPoint;
}

- (id) initWithNSPoint:(NSPoint)pNSPoint;
- (NSPoint) myNSPoint;
- (float)x;
- (float)y;

@end