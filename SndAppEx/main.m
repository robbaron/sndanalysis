//
//  main.m
//  SndAppEx
//
//  Created by Rob Baron on 5/27/14.
//  Copyright (c) 2014 Rob Baron. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
