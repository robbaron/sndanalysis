//
//  sndapp_AppDelegate.h
//  SndAppEx
//
//  Created by Rob Baron on 5/27/14.
//  Copyright (c) 2014 Rob Baron. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "sndapp_graph_view.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>

/*
#ifdef __LP64__
    typedef char int8;
    typedef short int16;
    typedef int int32;
    typedef long int64;
    typedef unsigned char uint8;
    typedef unsigned short uint16;
    typedef unsigned int uint32;
    typedef unsigned long long uint64;
#endif
*/

class snd_file
    {
protected:
    char *filename;
    FILE *fp;
    long size;
    double **channels;
    
    inline int open_file()
        {
        if(filename)
            {
            fp=fopen(filename,"rb");
            if(fp) return 1;
            }
        return 0;
        }
    inline unsigned long read(void *buf,int buf_size)
        {//this will be different under windows
        if(fp) { return fread(buf, buf_size, 1, fp); }
        return 0;
        }
    inline long long cnvt_le_2_be(long data, int size)
        {
        long long ret_val=0;
        long long tmp_data=data;
        int x=0;
        for(x=0; x<size; x++)
            {
            ret_val=ret_val<<8;
            ret_val=ret_val | (tmp_data & 0xFF);
            tmp_data = tmp_data>>8;
            }
        return ret_val;
        }
public:
    snd_file() {filename=NULL; size=0; channels=NULL;}
    ~snd_file()
        {
        if(filename) { free(filename); }
        //if(data) {free(data); }
        size=0;
        }
    inline int set_filename(const char *fname)
        {
            if(fname)
            {
                filename=(char *)malloc(sizeof(char)*strlen(fname));
                strncpy(filename, fname,strlen(fname));
                if(filename) return 1;
            }
            return 0;
        }
    virtual int read_file(const char *fname)=0;
    //virtual int write_file(char *fname)=0;
    virtual long get_data_size() { return size; }
    virtual double *get_data(int channel_number)
        {
        if(channels && channels[0]) return channels[0];
        return NULL;
        }
    virtual void fft(int channel,int start_index,int width,double *fft_out);
    virtual void wavelett(int channel, int data_len, double *wavelett_out);
    };

/*
**  C has changed its size for int and long on 64 bit architectures to something that makes
**  more sense (but probably broke a bit of code).  Always hated the "long long" solution.
**
**  type  ILP32 size  ILP32 align    LP64 size   LP64 align
**  char           1            1            1            1
**  short          2            2            2            2
**  int            4            4            4            4
**  long           4            4            8            8
**  long long      8            4            8            8
**  pointer        4            4            8            8
*/

struct chunk_header 
    {
    char chunk_id[4];
    unsigned int chunk_size;
    };

/*
** This header tells us we have a wave file.  It has the format of
**   4 Bytes containing RIFF or RIFX
**        RIFF indicates all integer fields are little endian
**        RIFX indicates all integer fields are big endian
**   4 byte integer -> size of file - 8 bytes
**   4 Bytes containing WAVE
*/
struct riff_header
    {
    //char RIFF[4];
    //long chunk_size;
    char WAVE[4];
    };

/*
** fmt chunk
**   4 Bytes containin "fmt "
**   4 Byte chunk size (16 in the case of PCM) RIFF LE...
**   2 Byte AutioFormat (0001=pcm, other number indicate compression scheme
**   2 Byte NumChannels
**   4 Byte SampleRate
**   4 Byte ByteRate
**   2 Byte BlockAlign
**   2 Byte BitsPerSample
**   2 Byte ExtraParamSize (not present if PCM)
**   ? Byte ExtraParam     (not present if PCM)
**
** only implement PCM for now!
*/

struct fmt_chunk
    {
    //char fmt[4];
    //long chunk_size;
    unsigned short AudioFormat; //0001 = pcm
    unsigned short NumChannels;
    unsigned int SampleRate;
    unsigned int ByteRate;
    unsigned short BlockAlign;
    unsigned short BitsPerSample;
    };

//This is not general enough, but will read the example wave files I have
//generated from audacity.
class wav_snd_file:public snd_file
    {
private:
        struct fmt_chunk fmt;
        char file_type[9];
        long file_len;
        int cnvt_to_big_endian;
        
        unsigned long read_chunk_header(struct chunk_header *ch)
        {
            //compiler dependent padding requires
            //  1) reading each value from the file
            //  2) packed structures, with alignment attributes
            //implementing 1 as it is much simpler to maintain cross platform
            return this->read(ch->chunk_id,4)+this->read(&(ch->chunk_size),sizeof(int));
        }
        int read_riff(struct chunk_header *ch)
            {
            struct riff_header riff;
            file_type[0]=0;
            
            this->read(riff.WAVE,4);
            if(strncmp(ch->chunk_id,"RIFF",4)==0 && strncmp(riff.WAVE,"WAVE",4)==0)
                {
                file_len=cnvt_le_2_be(ch->chunk_size,sizeof(int))+8;
                strncpy(&file_type[0], "RIFF", 4);
                strncpy(&file_type[4], riff.WAVE, 4);
                file_type[8]=0;
                cnvt_to_big_endian=0;
                return 1;
                }
            if(strncmp(ch->chunk_id,"RIFX",4)==0 && strncmp(riff.WAVE,"WAVE",4)==0)
                {
                file_len=ch->chunk_size+8;
                strncpy(&file_type[0], "RIFX", 4);
                strncpy(&file_type[4], riff.WAVE, 4);
                file_type[8]=0;
                cnvt_to_big_endian=0;
                return 1;
                }
            return 0;
            }
        int read_fmt_chunk(struct chunk_header *ch)
            {
            if(cnvt_to_big_endian) ch->chunk_size=cnvt_le_2_be(ch->chunk_size, sizeof(int));
            if(ch->chunk_size==16) //we have a pcm wave file
                {
                this->read(&fmt.AudioFormat,sizeof(short));
                this->read(&fmt.NumChannels,sizeof(short));
                this->read(&fmt.SampleRate, sizeof(int));
                this->read(&fmt.ByteRate,sizeof(int));
                this->read(&fmt.BlockAlign, sizeof(short));
                this->read(&fmt.BitsPerSample, sizeof(short));
                    
                if(cnvt_to_big_endian)
                    {
                    //fmt.chunk_size=cnvt_le_2_be(ch->chunk_size,sizeof(long));
                    fmt.AudioFormat=cnvt_le_2_be(fmt.AudioFormat, sizeof(short));
                    fmt.NumChannels=cnvt_le_2_be(fmt.NumChannels, sizeof(short));
                    fmt.SampleRate=cnvt_le_2_be(fmt.SampleRate, sizeof(int));
                    fmt.ByteRate=cnvt_le_2_be(fmt.ByteRate,sizeof(int));
                    fmt.BlockAlign=cnvt_le_2_be(fmt.BlockAlign, sizeof(short));
                    fmt.BitsPerSample=cnvt_le_2_be(fmt.BitsPerSample, sizeof(short));
                    }
                return 1;
                }
            return 0;
            }
        
        /* 
        ** The data chunk is a bit more difficult to read
        ** Format for PCM is as follows:
        **    char[4] contains "data"
        **    long    contains chunk size (may be little endian)
        **    data for sample 1, channel 1  (size from fmt chunk, may be little endian)
        **    data for sample 1, channel 2
        **    ...
        **    data for sample 1, channel n  (number of channels from fmt chunk
        **    data for sample 2, channel 1
        **    data for sample 2, channel 2
        **    ...
        **    data for sample 2, channel n
        **    ...
        **    data for sample n, channel 1
        **    data for sample n, channel 2
        **    ...
        **    data for sample n, channel n
        **
        ** my examples just have 1 data chunk per file
        */
        
        int read_data_chunk(struct chunk_header *ch)
            {
            if(cnvt_to_big_endian) ch->chunk_size=cnvt_le_2_be(ch->chunk_size,4);
            long sample_cnt = ch->chunk_size / fmt.BlockAlign;
            short data_size = fmt.BlockAlign / fmt.NumChannels;
            size=sample_cnt;
            //Allocate the arrays, 1 per channel
            channels=(double **)malloc(sizeof(void*)*fmt.NumChannels);
            for(int i=0; i<fmt.NumChannels; i++)
                {
                channels[i]=(double *)malloc(sample_cnt*sizeof(double));
                }
            //read the data and convert to a double from -1 to 1
            for(int x=0; x<sample_cnt; x++)
                {
                for(int i=0;i<fmt.NumChannels;i++)
                    {
                    short data16;
                    unsigned char data8;
                        
                    switch (data_size)
                        {
                        case 1:
                                this->read(&data8, 1);
                                channels[i][x]=(data8/(double)255)*2-1;
                                break;
                        case 2:
                                this->read(&data16,2);
                                if(cnvt_to_big_endian) data16=cnvt_le_2_be(data16,2);
                                channels[i][x]=(data16/(double)SHRT_MAX);
                                break;
                        default:
                                ;//ERROR
                        }
                    }
                }
            //this->read(&data,8);
            return 1;
            }
public:
    wav_snd_file() { }
    virtual ~wav_snd_file() {}
    virtual int read_file(const char *fname);
    virtual int write_file(const char *fname);
    virtual double *get_data(int channel,long num_points);
    };

@interface sndapp_AppDelegate : NSObject <NSApplicationDelegate> {
}
@property (assign) IBOutlet graph_view *gr_view;
@property (assign) IBOutlet NSWindow *window;

- (void)attach_soundfile: (const char *) this_file;


@end

extern snd_file *sf;
extern sndapp_AppDelegate *myApp;
