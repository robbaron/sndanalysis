//
//  sndapp_AppDelegate.m
//  SndAppEx
//
//  Created by Rob Baron on 5/27/14.
//  Copyright (c) 2014 Rob Baron. All rights reserved.
//

#import "sndapp_AppDelegate.h"
#include <string.h>
#include <fftw3.h>

#include <stdio.h>
#include <math.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_test.h>

#include <gsl/gsl_wavelet.h>
#include <gsl/gsl_wavelet2d.h>

snd_file *sf;
sndapp_AppDelegate *myApp;


@implementation sndapp_AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
    {
    // Insert code here to initialize your application
    myApp=self;

    //How to get the main view
    //NSView *main_view;
    //main_view=[[self window] contentView];
    
    //How to get subviews
    //subviews=[main_view subviews];
    }

- (void)attach_soundfile: (const char *) filename;
    {
    sf = new wav_snd_file;
    sf->read_file(filename);
    long n=sf->get_data_size();
    double *data=sf->get_data(0);
    [[self gr_view] set_data:data size:n];
    [[[self window] contentView] setNeedsDisplay:TRUE ];
    }

@end

void snd_file::fft(int channel,int data_len, int win_width, double *fft_out)
    {
    double *data_in;
    fftw_complex *data_out;
    double *win_func;
    fftw_plan plan_forward;
    
    data_in=(double *)fftw_malloc(sizeof(double)*win_width);
    data_out=(fftw_complex *)fftw_malloc(sizeof(double)*win_width);
    win_func=(double*)malloc(sizeof(double)*win_width);
    
    //calculate the window fucntion percents (these will be used over and over again)
    for(int x=0; x<data_len; x++)
        {
        //if()
            {
            win_func[x]=1.0;
            }
        //else if()
        //    {
        //    window_func[x]= /* some func */;
        //    }
        }
    
    plan_forward = fftw_plan_dft_r2c_1d ( win_width, data_in, data_out, FFTW_ESTIMATE );
    
    
    //1st break channel into "window"s
    //Apply a windowing function to minimize artifacts of the FFT
    //run the FFT on each window - amplitude along real plane, phase along imagninary plane
    fftw_execute ( plan_forward );

    //take the complex data out and square it (make it real again:).
    }

void snd_file::wavelett(int channel, int data_len, double *wavelett_out)
    {
    gsl_wavelet_workspace *work;
    gsl_vector *v1, *v2, *vdelta;
    gsl_vector_view v;
    gsl_wavelet *w;
    

    size_t i;

    int N;
    int stride;
    const gsl_wavelet_type *T;
        
    //v = gsl_vector_view_array_with_stride (data, stride, N);
    //v1 = &(v.vector);
    
    //for (i = 0; i < N; i++)
    //    {
    //    gsl_vector_set (v1, i, urand ());
    //    }
    
    //v2 = gsl_vector_alloc (N);
    //gsl_vector_memcpy (v2, v1);
    
    vdelta = gsl_vector_alloc (data_len);
    work = gsl_wavelet_workspace_alloc (data_len);
    w = gsl_wavelet_alloc (gsl_wavelet_daubechies, data_len);
    
    gsl_wavelet_transform_forward (w, v2->data, v2->stride, v2->size, work);
    gsl_wavelet_transform_inverse (w, v2->data, v2->stride, v2->size, work);
    
    for (i = 0; i < data_len; i++)
        {
        double x1 = gsl_vector_get (v1, i);
        double x2 = gsl_vector_get (v2, i);
        gsl_vector_set (vdelta, i, fabs (x1 - x2));
        }
    
        {
        double x1, x2;
        i = gsl_vector_max_index (vdelta);
        x1 = gsl_vector_get (v1, i);
        x2 = gsl_vector_get (v2, i);
        
        gsl_test (fabs (x2 - x1) > N * 1e-15,
                  "%s(%d), n = %d, stride = %d, maxerr = %g",
                  gsl_wavelet_name (w), data_len, N, stride, fabs (x2 - x1));
        }
    
    if (stride > 1)
        {
        int status = 0;
        
        for (i = 0; i < N * stride; i++)
            {
            if (i % stride == 0)
                continue;
            
            status |= (wavelett_out[i] != (12345.0 + i));
            }
        
        gsl_test (status, "%s(%d) other data untouched, n = %d, stride = %d",
                  gsl_wavelet_name (w), data_len, N, stride);
        }
    
    gsl_wavelet_workspace_free (work);
    gsl_wavelet_free (w);
    gsl_vector_free (vdelta);
    gsl_vector_free (v2);
    }

int wav_snd_file::read_file(const char *fname)
    {
    int done=0;
    this->set_filename(fname);
    this->open_file();
    while(!done)
        {
        struct chunk_header ch;
        if(this->read_chunk_header(&ch)>0)
            {
            if(strncmp(ch.chunk_id, "RIFF", 4)==0 ||
               strncmp(ch.chunk_id, "RIFX", 4)==0)
                {
                this->read_riff(&ch);
                }
            else if (strncmp(ch.chunk_id, "fmt ", 4)==0)
                {
                this->read_fmt_chunk(&ch);
                }
            else if (strncmp(ch.chunk_id, "data", 4)==0)
                {
                this->read_data_chunk(&ch);
                }
            }
        else
            {
            done=1;
            }
        }
    return 1;
    }

int wav_snd_file::write_file(const char *fname)
    {
    
    return 1;
    }

double *wav_snd_file::get_data(int channel,long num_points)
    {
    return NULL;
    }

