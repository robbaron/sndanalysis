//
//  sndapp_view.m
//  SndAppEx
//
//  Created by Rob Baron on 5/29/14.
//  Copyright (c) 2014 Rob Baron. All rights reserved.
//

#import "sndapp_view.h"
#import "sndapp_AppDelegate.h"

@implementation sndapp_view

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (IBAction)sendFileButtonAction:(id)sender{
    
    NSOpenPanel* openDlg = [NSOpenPanel openPanel];
    
    // Enable the selection of files in the dialog.
    [openDlg setCanChooseFiles:YES];
    
    // Enable the selection of directories in the dialog.
    [openDlg setCanChooseDirectories:NO];
    
    // Change "Open" dialog button to "Select"
    [openDlg setPrompt:@"Select"];
    
    // Display the dialog.  If the OK button was pressed,
    // process the files.
    if ( [openDlg runModal] == NSOKButton )
    {
        // Get an array containing the full filenames of all
        // files and directories selected.
        NSArray* files = [openDlg filenames];
        
        // Loop through all the files and process them.
        for( int i = 0; i < [files count]; i++ )
        {
            NSString* fileName = [files objectAtIndex:i];
            NSLog(@"file: %@", fileName);

            // Do something with the filename.
            [disp_filename setStringValue:[files objectAtIndex:i]];
            [myApp attach_soundfile: [fileName UTF8String]];
        }
    }
}

- (void)drawRect:(NSRect)pNSRect { }
@end

@implementation graph_view


- (id)initWithFrame:(NSRect)pNsrectFrameRect {

    if ((self = [super initWithFrame:pNsrectFrameRect]) == nil) {
        return self;
    } // end if
    n=0;
    data=NULL;
	//myMutaryOfBrushStrokes = [[NSMutableArray alloc]init];
    
    // initialise random numebr generator used in drawRect for creating colours etc.
    //srand(time(NULL));
    
    return self;
} // end initWithFrame



/*
**-(void)mouseDown:(NSEvent *)pTheEvent {
**    
**    myMutaryOfPoints = [[NSMutableArray alloc]init];
**    [myMutaryOfBrushStrokes addObject:myMutaryOfPoints];
**    
**    NSPoint tvarMousePointInWindow = [pTheEvent locationInWindow];
**    NSPoint tvarMousePointInView   = [self convertPoint:tvarMousePointInWindow fromView:nil];
**    MyPoint * tvarMyPointObj      = [[MyPoint alloc]initWithNSPoint:tvarMousePointInView];
**    
**    [myMutaryOfPoints addObject:tvarMyPointObj];
**    
**} // end mouseDown
*/

/*
**-(void)mouseDragged:(NSEvent *)pTheEvent {
**    
**    NSPoint tvarMousePointInWindow = [pTheEvent locationInWindow];
**    NSPoint tvarMousePointInView   = [self convertPoint:tvarMousePointInWindow fromView:nil];
**    MyPoint * tvarMyPointObj      = [[MyPoint alloc]initWithNSPoint:tvarMousePointInView];
**    
**    [myMutaryOfPoints addObject:tvarMyPointObj];
**    
**    [self setNeedsDisplay:YES];
**    
**} // end mouseDragged
*/


/*
**-(void)mouseUp:(NSEvent *)pTheEvent {
**
**    NSPoint tvarMousePointInWindow = [pTheEvent locationInWindow];
**    NSPoint tvarMousePointInView   = [self convertPoint:tvarMousePointInWindow fromView:nil];
**    MyPoint * tvarMyPointObj      = [[MyPoint alloc]initWithNSPoint:tvarMousePointInView];
**
**    [myMutaryOfPoints addObject:tvarMyPointObj];
**
**    [self setNeedsDisplay:YES];
**
**} // end mouseUp
*/

/*
**- (float)randVar;
**{
**    return ( (float)(rand() % 10000 ) / 10000.0);
**} // end randVar
*/

/*
**  Need to find the width and height of the view
**  Find the number of point in the data set
**  Find the absolute min and max of the data set

**  delta_x=view_width/number_of_points
**
**  for 1 byte data
**    delta_y=hieght/(256)
**    offset=delta_y*128
**
**  plot a point x=sample_number*delta_x
**               y=data[sample_number]*delta_y+offset
**
**  plot the x-axis
**
**
**
**
*/

- (void)drawRect:(NSRect)pNSRect {
    // colour the background white
    [[NSColor whiteColor] set];		// this is Cocoa
    NSRectFill( pNSRect );

    [self drawBorder: [self bounds] ];
    [self drawAxis: [self bounds] ];
    [self plot: [self bounds]];
/*    if ([myMutaryOfBrushStrokes count] == 0) {
        return;
    } // end if
    
    // This is Quartz
    NSGraphicsContext * tvarNSGraphicsContext = [NSGraphicsContext currentContext];
    CGContextRef      tvarCGContextRef     = (CGContextRef) [tvarNSGraphicsContext graphicsPort];
    
    NSUInteger tvarIntNumberOfStrokes = [myMutaryOfBrushStrokes count];
    
    NSUInteger i;
    for (i = 0; i < tvarIntNumberOfStrokes; i++) {
        
        CGContextSetRGBStrokeColor(tvarCGContextRef,0.50,0.50,0.50,1.0);
        CGContextSetLineWidth(tvarCGContextRef, 5 );
        
        myMutaryOfPoints = [myMutaryOfBrushStrokes objectAtIndex:i];
        
        NSUInteger tvarIntNumberOfPoints = [myMutaryOfPoints count];    // always >= 2
        MyPoint * tvarLastPointObj      = [myMutaryOfPoints objectAtIndex:0];
        CGContextBeginPath(tvarCGContextRef);
        CGContextMoveToPoint(tvarCGContextRef,[tvarLastPointObj x],[tvarLastPointObj y]);
        
        NSUInteger j;
        for (j = 1; j < tvarIntNumberOfPoints; j++) {  // note the index starts at 1
            MyPoint * tvarCurPointObj = [myMutaryOfPoints objectAtIndex:j];
            CGContextAddLineToPoint(tvarCGContextRef,[tvarCurPointObj x],[tvarCurPointObj y]);
        } // end for
        CGContextDrawPath(tvarCGContextRef,kCGPathStroke);
    } // end for
*/
}
- (void) set_data: (double *) d size:(long)size
    {
    //TODO: implmenet mutexes on the data - for now just copy
    //as this function may be operating in a different thread than the drawrect
    n=size;
    data = (double *)malloc(n*sizeof(double));
    for(int i=0; i<n; i++)
        {
        data[i]=d[i];
        }
    }

- (void) plot: (NSRect) bounds
    {
    if(n>0)
        {
        // This is Quartz
        NSGraphicsContext * tvarNSGraphicsContext = [NSGraphicsContext currentContext];
        CGContextRef      tvarCGContextRef     = (CGContextRef) [tvarNSGraphicsContext graphicsPort];
        CGContextSetRGBStrokeColor(tvarCGContextRef,0.0,0.0,0.0,1.0);
        CGContextSetLineWidth(tvarCGContextRef, 1 );

        CGContextBeginPath(tvarCGContextRef);
        CGContextMoveToPoint(tvarCGContextRef,0,bounds.size.height/2);
        for(int i=0; i<n; i++)
            {
            int x=bounds.size.width*((double)i/n);
            int y=bounds.size.height*((data[i]+1)/2);
            CGContextAddLineToPoint(tvarCGContextRef,x, y);
            }
        CGContextDrawPath(tvarCGContextRef,kCGPathStroke);
        }
    }

- (void) drawAxis: (NSRect) bounds
    {
    // This is Quartz
    NSGraphicsContext * tvarNSGraphicsContext = [NSGraphicsContext currentContext];
    CGContextRef      tvarCGContextRef     = (CGContextRef) [tvarNSGraphicsContext graphicsPort];
    CGContextSetRGBStrokeColor(tvarCGContextRef,0.0,0.0,0.0,1.0);
    CGContextSetLineWidth(tvarCGContextRef, 1 );

    CGContextBeginPath(tvarCGContextRef);
    CGContextMoveToPoint(tvarCGContextRef,0,bounds.size.height/2);
    CGContextAddLineToPoint(tvarCGContextRef,bounds.size.width, bounds.size.height/2);
    CGContextDrawPath(tvarCGContextRef,kCGPathStroke);
    }

- (void) drawBorder: (NSRect) bounds {
    // This is Quartz
    NSGraphicsContext * tvarNSGraphicsContext = [NSGraphicsContext currentContext];
    CGContextRef      tvarCGContextRef     = (CGContextRef) [tvarNSGraphicsContext graphicsPort];
    CGContextSetRGBStrokeColor(tvarCGContextRef,0.0,0.0,0.0,1.0);
    CGContextSetLineWidth(tvarCGContextRef, 1 );
    
    CGContextBeginPath(tvarCGContextRef);
    CGContextMoveToPoint(tvarCGContextRef,0,0);
    CGContextAddLineToPoint(tvarCGContextRef,bounds.size.width, 0);
    CGContextAddLineToPoint(tvarCGContextRef, bounds.size.width, bounds.size.height);
    CGContextAddLineToPoint(tvarCGContextRef, 0, bounds.size.height);
    CGContextAddLineToPoint(tvarCGContextRef, 0, 0);
    CGContextDrawPath(tvarCGContextRef,kCGPathStroke);
    
}

@end


@implementation MyPoint

- (id) initWithNSPoint:(NSPoint)pNSPoint;
{
    if ((self = [super init]) == nil) {
        return self;
    } // end if
    
    myNSPoint.x = pNSPoint.x;
    myNSPoint.y = pNSPoint.y;
    
    return self;
    
} // end initWithNSPoint

- (NSPoint) myNSPoint;
{
    return myNSPoint;
} // end myNSPoint

- (float)x;
{
    return myNSPoint.x;
} // end x

- (float)y;
{
    return myNSPoint.y;
} // end y


@end

