//
//  sndapp_view.h
//  SndAppEx
//
//  Created by Rob Baron on 5/29/14.
//  Copyright (c) 2014 Rob Baron. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "sndapp_AppDelegate.h"
#import "sndapp_graph_view.h"

@interface sndapp_view : NSView {
    char *filename;
    IBOutlet NSTextFieldCell *disp_filename;
}
- (IBAction)sendFileButtonAction:(id)sender;


@end

